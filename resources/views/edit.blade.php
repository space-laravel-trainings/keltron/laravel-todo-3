@extends('layout')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('style')
<style>
form{
    text-align:center;
    border-style: dashed;
    width:50%;
    height:50%;
    margin-left:300px;
    background-color: purple;
    color:white;
}
</style>
@endsection

@section('content')
    <form method="post"action='/task/{{$row->id}}'>
      @csrf
      @method('put')
      <h1> ADD TASK</h1>
      <br><br>
      <label>Task:</label>
      <input style="margin-left:130px" type="text" name="task" value="{{$row->task}}" placeholder="Enter Task" required><br><br>
      <label>Task Desciption:</label>
      <input style="margin-left:50px"type="text" name="taskdescription"  value="{{$row->taskdescription}}" placeholder="Enter Task Desciption" required><br><br>
      <label>Status:</label>


      <input style="margin-left:150px" type="radio"  name="status"  @if($row->status==0) checked @endif value="0">Yes

      <input type="radio" name="status"  @if($row->status==1) checked @endif value="1">No

      <br><br>

      <input type="submit" name="submit" value="Save">
          </form>
  @endsection
