@extends('layout')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('style')
<style>
form{
    text-align:center;
    border-style: dashed;
    width:50%;
    height:50%;
    margin-left:300px;
    background-color: grey;
    color:white;
}
</style>
@endsection

@section('content')
    <form method="post"action='/task'>
      @csrf
      <h1> ADD TASK</h1>
      <br><br>
      <label>Task:</label>
      <input style="margin-left:130px" type="text" name="task" placeholder="Enter Task" required><br><br>
      <label>Task Desciption:</label>
      <input style="margin-left:50px"type="text" name="taskdescription" placeholder="Enter Task Desciption" required><br><br>
      <label>Status:</label>
      <input style="margin-left:150px" type="radio" value="0" name="status">Yes
      <input type="radio" value="1" name="status">No
      <br><br>
      <input type="submit" name="submit" value="Save">
          </form>
  @endsection
