@extends('layout')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('style')
<style>
    h1{
        text-align:center;
    }
    form{
        text-align:center;
    }
    table{
      margin-left:100px;
    }
    </style>
    @endsection

    @section('content')
  <h1>TO-DO LIST</h1>
    <form method=post>
      <label>TASK</label>
      <input style="width:35%" type="text" name="text"><br><br>
      <a href="/task/create">ADD TASK</a><br>
      <br>
      <table border="1">
        <tr>
        <th>Tasks</th>
        <th>Taskdescription</th>
        <th>Status</th>
        <th>Action</th>
  </tr>

  @foreach ($t as $row)
  <tr>
    <td> {{ $row->task }}</td>


    <td> {{ $row->taskdescription }}</td>

    <td>
    @php

    if($row->status==0) echo"TODO";
  elseif ($row->status==1) echo"DONE";

    @endphp </td>

    <td>
      <a href="/task/{{$row->id}}/edit">EDIT</a>
      <form method="post" action="/task/{{$row->id}}">
        @csrf
        @method('delete')
    <input type="submit" value="Delete" name="delete" </td>
</form>
  </tr>
  @endforeach


</table>
@endsection
